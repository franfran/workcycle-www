from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import APIException
from mailchimp3 import MailChimp
from django.conf import settings
import hashlib

def validate_request(request, keys):
    for i in keys:
        if i not in request.data:
            raise APIException('missing {}'.format(i))

@api_view(['GET', 'POST'])
def subscription(request):
    #could allow GET/DEL in api_view for other operation
    if request.method == 'POST':
        keys = {'email'}
        validate_request(request, keys)
        email = request.data['email']
        hash=hashlib.md5(email.lower().encode()).hexdigest()
        mcsetting = getattr(settings, 'MAILCHIMP', None)

        try:
            mc = MailChimp(mcsetting['USERNAME'], mcsetting['API_KEY'])
            mc.lists.members.create_or_update(mcsetting['SUBSCRIPTION_LIST_ID'], hash, {
                'email_address': email,
                'status_if_new': 'subscribed',
            })
            return Response({"status": True}, status.HTTP_200_OK)
        except Exception as inst:
            return Response({"status": False}, status.HTTP_200_OK)
    else:
        return Response({}, status.HTTP_200_OK)
