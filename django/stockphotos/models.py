from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible

from wagtail.wagtailsnippets.models import register_snippet
from wagtail.wagtailadmin.edit_handlers import FieldPanel

from wagtailimages_optional_title_chooserpanel.edit_handlers import ImageOptionalTitleChooserPanel

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

@register_snippet
# provide equivalent __unicode__ and __str__ methods on Python 2
@python_2_unicode_compatible
class StockPhoto(models.Model):
    photo = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    author = models.CharField(max_length=255, blank=True)
    license_name = models.CharField(max_length=255, blank=True)
    license_url = models.URLField(blank=True)
    photo_source_url = models.URLField(blank=True)

    panels = [
        ImageOptionalTitleChooserPanel('photo'),
        FieldPanel('photo_source_url'),
        FieldPanel('author'),
        FieldPanel('license_name'),
        FieldPanel('license_url'),
    ]

    def __str__(self):
        title = 'photo removed - ' + str(self.id)
        if hasattr(self.photo, 'title'):
            title = self.photo.title
        return title

    class Meta:
        verbose_name = "StockPhoto"

# Delete the source image file when an image is deleted


@receiver(post_delete, sender=StockPhoto)
def image_delete(sender, instance, **kwargs):
    # delete the photo if it exist
    if not instance.photo is None:
        instance.photo.delete(False)

def get_stockphoto_model_string():
    """
    refer to wagtailimages.Image get_image_model_string()
    """
    return getattr(settings, 'STOCKPHOTO_MODEL', 'stockphotos.StockPhoto')

def get_stockphoto_model():
    from django.apps import apps

    model_string = get_stockphoto_model_string()    
    try:
        return apps.get_model(model_string)
    except ValueError:
        raise ImproperlyConfigured("STOCKPHOTO_MODEL must be of the form 'app_label.model_name'")
    except LookupError:
        raise ImproperlyConfigured(
            "STOCKPHOTO_MODEL refers to model '%s' that has not been installed" % model_string
        )
