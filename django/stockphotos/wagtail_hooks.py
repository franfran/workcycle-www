from wagtail.contrib.modeladmin.options import ThumbnailMixin, ModelAdmin, modeladmin_register

from .models import StockPhoto


class StockPhotoModelAdmin(ThumbnailMixin, ModelAdmin):
    model = StockPhoto
    menu_label = 'StockPhoto'  # ditch this to use verbose_name_plural from model
    menu_icon = 'image'  # change as required
    menu_order = 200  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False
    list_display = ('admin_thumb', 'photo',)
    search_fields = ('photo__title',)
    thumb_image_field_name = 'photo'
    thumb_image_filter_spec = 'max-100x100'

# Now you just need to register your customised ModelAdmin class with Wagtail
modeladmin_register(StockPhotoModelAdmin)
