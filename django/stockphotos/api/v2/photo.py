# getting the image_url by our own
# Ref: https://github.com/torchbox/wagtail/issues/2087

from django.core.urlresolvers import reverse
from django.conf import settings

def generate_image_url(image, filter_spec, request):
    # we have to import in runtime, otherwise, the image model is not ready yet
    # since the serve has the line 'serve = ServeView.as_view()'
    from wagtail.wagtailimages.views.serve import generate_signature
    signature = generate_signature(image.id, filter_spec)
    url = reverse('stockphotos_api_serve', args=(signature, image.id, filter_spec))

    #generate the full URL based on the incoming request
    #though we have WAGTAILAPI_BASE_URL to use, but we want to generate it dynamically
    url = request.build_absolute_uri(url)

    return url
