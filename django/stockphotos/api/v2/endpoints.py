from __future__ import absolute_import, unicode_literals

from wagtail.api.v2.endpoints import BaseAPIEndpoint
from wagtail.api.v2.filters import FieldsFilter, OrderingFilter, SearchFilter

from ...models import get_stockphoto_model
from .serializers import StockPhotoSerializer


class StockPhotosAPIEndpoint(BaseAPIEndpoint):
    base_serializer_class = StockPhotoSerializer
    filter_backends = [FieldsFilter, OrderingFilter, SearchFilter]
    body_fields = BaseAPIEndpoint.body_fields + ['title']
    meta_fields = BaseAPIEndpoint.meta_fields
    listing_default_fields = BaseAPIEndpoint.listing_default_fields + ['title']
    nested_default_fields = BaseAPIEndpoint.nested_default_fields + ['title']
    name = 'stockphotos'
    model = get_stockphoto_model()
