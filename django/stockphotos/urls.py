from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from wagtail.wagtailimages.views.serve import ServeView

urlpatterns = [
    url(r'^stockphotos-api/([^/]*)/(\d*)/([^/]*)/[^/]*$', ServeView.as_view(), name='stockphotos_api_serve'),
]
