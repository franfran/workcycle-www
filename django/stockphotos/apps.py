from django.apps import AppConfig


class StockphotosConfig(AppConfig):
    name = 'stockphotos'
