from __future__ import absolute_import, unicode_literals

from .base import *

import threading

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'n9kfnu@599+l092$87ic5$@!wwzaq#_d)vn4%dl*!=@d211c40'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'workcycle_dev',
        'USER': 'root',
        'PASSWORD': 'password',
        'HOST': 'workcycle_db_1',
        'PORT': '3306',
    }
}

"""
Increase the stack size to prevent segfault on Alpine docker using musl instead of glibc 
https://github.com/python-pillow/Pillow/issues/1935
"""
threading.stack_size(4*80*1024)

try:
    from .local import *
except ImportError:
    pass
