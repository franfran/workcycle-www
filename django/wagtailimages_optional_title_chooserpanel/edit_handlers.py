from __future__ import absolute_import, unicode_literals

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel


class ImageOptionalTitleChooserPanel(ImageChooserPanel):

    def __init__(self, field_name):
        super(ImageOptionalTitleChooserPanel, self).__init__(field_name)

    def bind_to_model(self, model):
        image = model._meta.get_field(self.field_name)
        if image.get_internal_type() == "ForeignKey":
            image = image.rel.to

        title = image._meta.get_field('title')
        title.blank = True

        return super(ImageOptionalTitleChooserPanel, self).bind_to_model(model)
