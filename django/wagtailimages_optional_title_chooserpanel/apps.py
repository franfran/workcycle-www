from __future__ import unicode_literals

from django.apps import AppConfig


class WagtailimagesOptionalTitleChooserpanelConfig(AppConfig):
    name = 'wagtailimages_optional_title_chooserpanel'
