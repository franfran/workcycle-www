from django.contrib.staticfiles.templatetags.staticfiles import static
from django.utils.html import format_html_join
#from django.core import urlresolvers

from wagtail.wagtailcore import hooks


@hooks.register('insert_editor_js')
def editor_js():
    js_files = [
        static(
            'wagtailimages_optional_title_chooserpanel/js/image-optional-title-default-value.js'),
    ]
    js_includes = format_html_join(
        '\n', '<script src="{0}"></script>',
        ((filename, ) for filename in js_files)
    )
    return js_includes
