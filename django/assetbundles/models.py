from django.db import models

from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailcore.fields import StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel
from wagtail.wagtailimages.blocks import ImageChooserBlock

from stockphotos.models import StockPhoto
from stockphotos.api.v2.photo import generate_image_url
from django.contrib import admin
from foreignkeyfieldwiththumbnailchooser.widgets import AdminNestedObjectsFieldWithThumbnailChooser

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

# Create your models here.

BUNDLE_TYPE = (
    ('home', 'Home'),
    ('break', 'Break'),
    ('finish', 'Finish'),
)


class AssetBundlePage(Page):
    bundletype = models.CharField(
        max_length=6, choices=BUNDLE_TYPE, default='home')
    photo = models.ForeignKey(
        'stockphotos.StockPhoto',
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    message = RichTextField(blank=True)
    readmore_label = models.CharField(blank=True, max_length=25)
    readmore_url = models.URLField(blank=True)

    api_fields = [
        'photo_url',
    ]

    request = None

    @classmethod
    def inject_request(cls, request):
        cls.request = request

    @property
    def photo_url(self):
        filter_spec = 'fill-1920x1080'
        photo = getattr(self, 'photo', None) #photo field from AssetBundle
        photo = getattr(photo, 'photo', None) #photo field from StockPhoto
        url = generate_image_url(photo, filter_spec, self.request)

        return url

# Editor panels configuration
AssetBundlePage.content_panels = Page.content_panels + [
    FieldPanel('bundletype'),
    FieldPanel(
        'photo', widget=AdminNestedObjectsFieldWithThumbnailChooser(model=StockPhoto, thumbnail_field_name='photo')),
    # SnippetChooserPanel('photo'),
    FieldPanel('message'),
    FieldPanel('readmore_label'),
    FieldPanel('readmore_url'),
    # StreamFieldPanel('stest'),
]

subpage_types = []

def get_assetbundle_model_string():
    """
    refer to wagtailimages.Image get_image_model_string()
    """
    return getattr(settings, 'ASSETBUNDLE_MODEL', 'assetbundles.AssetBundlePage')

def get_assetbundle_model():
    from django.apps import apps

    model_string = get_assetbundle_model_string()    
    try:
        return apps.get_model(model_string)
    except ValueError:
        raise ImproperlyConfigured("ASSETBUNDLE_MODEL must be of the form 'app_label.model_name'")
    except LookupError:
        raise ImproperlyConfigured(
            "ASSETBUNDLE_MODEL refers to model '%s' that has not been installed" % model_string
        )
