from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register


from .models import AssetBundlePage
from foreignkeyfieldwiththumbnailchooser.models import NestedObjectsThumbnailMixin


class AssetBundlePageModelAdmin(NestedObjectsThumbnailMixin, ModelAdmin):
    model = AssetBundlePage
    menu_label = 'AssetBundlePage'
    menu_icon = 'doc-full-inverse'
    menu_order = 200  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ('title', 'admin_thumb_from_nested_objects',
                    'bundletype', 'live',)
    list_filter = ('live', 'bundletype',)
    search_fields = ('title', 'photo__title',)
    nested_object_thumb_image_field_name = 'photo__photo'
    thumb_image_filter_spec = 'max-100x100'


modeladmin_register(AssetBundlePageModelAdmin)
