from __future__ import absolute_import, unicode_literals

from wagtail.api.v2.endpoints import BaseAPIEndpoint
from wagtail.api.v2.filters import FieldsFilter, OrderingFilter, SearchFilter

from ...models import get_assetbundle_model
from .serializers import AssetBundleSerializer
from django.db.models.query import QuerySet, Q


class AssetBundlesAPIEndpoint(BaseAPIEndpoint):
    base_serializer_class = AssetBundleSerializer
    filter_backends = [FieldsFilter, OrderingFilter, SearchFilter]
    body_fields = BaseAPIEndpoint.body_fields + ['title', 'bundletype', 'photo', 'message', 'readmore_url', 'readmore_label']
    meta_fields = BaseAPIEndpoint.meta_fields
    listing_default_fields = BaseAPIEndpoint.listing_default_fields + ['title', 'bundletype', 'photo', 'message', 'readmore_url', 'readmore_label']
    nested_default_fields = BaseAPIEndpoint.nested_default_fields + ['title', 'bundletype', 'photo', 'message', 'readmore_url', 'readmore_label']
    name = 'assetbundles'
    model = get_assetbundle_model()
    known_query_parameters = BaseAPIEndpoint.known_query_parameters.union([
        'version',
    ])

    def get_queryset(self):
        #since a model cannot get the request object, we have to pass around
        self.model.inject_request(self.request)
        homeBundle = self.model.objects.filter(live=True, expired=False, bundletype='home').order_by('-go_live_at', '-id')[:1]
        breakBundle = self.model.objects.filter(live=True, expired=False, bundletype='break').order_by('-go_live_at', '-id')[:1]
        finishBundle = self.model.objects.filter(live=True, expired=False, bundletype='finish').order_by('-go_live_at', '-id')[:1]

        return self.model.objects.filter(Q(id=homeBundle.values('id')) | Q(id=breakBundle.values('id')) | Q(id=finishBundle.values('id'))).order_by('id')
