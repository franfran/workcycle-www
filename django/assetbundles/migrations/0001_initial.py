# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-15 06:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('stockphotos', '0001_initial'),
        ('wagtailcore', '0032_add_bulk_delete_page_permission'),
    ]

    operations = [
        migrations.CreateModel(
            name='AssetBundlePage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('bundletype', models.CharField(choices=[(b'home', b'Home'), (b'break', b'Break'), (b'finish', b'Finish')], default=b'home', max_length=6)),
                ('message', wagtail.wagtailcore.fields.RichTextField(blank=True)),
                ('readmore_url', models.URLField()),
                ('photo', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='stockphotos.StockPhoto')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
    ]
