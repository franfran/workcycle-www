from __future__ import absolute_import, unicode_literals

import json

from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from wagtail.wagtailadmin.widgets import AdminChooser, AdminPageChooser
from wagtail.wagtailsnippets.widgets import AdminSnippetChooser


class AdminNestedObjectsFieldWithThumbnailChooser(AdminChooser):

    def __init__(self, model, thumbnail_field_name, **kwargs):
        super(AdminNestedObjectsFieldWithThumbnailChooser, self).__init__(**kwargs)

        self.target_model = model
        self.thumbnail_field_name = thumbnail_field_name

        name = self.target_model._meta.verbose_name
        self.choose_one_text = _('Choose %s') % name
        self.choose_another_text = _('Choose another %s') % name
        self.link_to_chosen_text = _('Edit this %s') % name

        self.model_type = 'snippet'
        if hasattr(self.target_model, 'url_path'):
            self.model_type = 'page'

    def render_html(self, name, value, attrs):
        instance, value = self.get_instance_and_id(self.target_model, value)
        original_field_html = super(
            AdminNestedObjectsFieldWithThumbnailChooser, self).render_html(name, value, attrs)

        print 'hello1'
        print instance
        print 'hello2'
        print self.target_model
        print 'hello3'
        print value
        #thumbnail = getattr(instance, self.thumbnail_field_name)
        thumbnail = None
        if instance is None:
            print 'it is none!!'
        else:
            thumbnail = getattr(instance, self.thumbnail_field_name)
#        thumbnail = instance.get_field(self.thumbnail_field_name)
#        print thumbnail.get_internal_type()

#        if thumbnail.get_internal_type() == "ForeignKey":
#            thumbnail = thumbnail.rel.to

        return render_to_string("foreignkeyfieldwiththumbnailchooser/widgets/item_chooser.html", {
            'widget': self,
            'model_opts': self.target_model._meta,
            'original_field_html': original_field_html,
            'attrs': attrs,
            'value': value,
            'thumbnail': thumbnail,
            'model_type': self.model_type,
            'item': instance,
        })

    def render_js_init(self, id_, name, value):
        model = self.target_model

        if self.model_type == 'snippet':
            c = AdminSnippetChooser(self.target_model)
        else:
            c = AdminPageChooser(target_models=[self.target_model])

        r = c.render_js_init(id_, name, value)

        return r
