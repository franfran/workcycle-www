from wagtail.contrib.modeladmin.options import ThumbnailMixin


class NestedObjectsThumbnailMixin(ThumbnailMixin):

    def admin_thumb_from_nested_objects(self, obj):
        #e.g. thumb_image_field_name = 'user1__profile1__company1__photo1'
        keys = self.nested_object_thumb_image_field_name.split('__')
        first_key = keys.pop(0)
        last_key = keys[-1]
        image1 = getattr(obj, first_key, None)

        # getting to the last object
        if len(keys) > 1:
            for x in keys:
                image1 = getattr(image1, x, None)

        self.thumb_image_field_name = last_key
        image = self.admin_thumb(image1)

        return image

    admin_thumb_from_nested_objects.short_description = ThumbnailMixin.thumb_col_header_text
